import java.util.Scanner;

public class hangman {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Please enter the word the other player will have to guess");
		String word = reader.nextLine();
		System.out.print("\033[H\033[2J");
		word = word.toUpperCase();
		
		runGame(word);
	}
	public static int isLetterInWord(String word, char c){
		for(int i = 0; i < word.length(); i++){
			if(word.charAt(i) == c){
				return i;
			}
		}
		return -1;
	}
	public static char toUpperCase(char c){
		c = Character.toUpperCase(c);
		return c;
	}
	public static void printWork(String word, boolean[] letters) {
		String[] newWord = new String[letters.length];
		char[] oldWord = word.toCharArray();
		for(int i = 0; i < letters.length; i++){
			if(letters[i]){
				newWord[i] = Character.toString(oldWord[i]);
			}
			else newWord[i] = "_";
		}
		String finalString = "";
		for(int i = 0; i < newWord.length; i++){
			finalString = finalString + newWord[i];
		}
		System.out.println("Your guess so far is:" + finalString);
	}
	public static void runGame(String word){
		Scanner reader = new Scanner(System.in);
		
		boolean targetReached = false;
		boolean[] lettersPosition = new boolean[word.length()];
		int attempts = 5;
		
		for(int i = 0; i < word.length(); i++){
			lettersPosition[i] = false;
		}
		
		while(attempts >= 0 && !targetReached){
			targetReached = true;
			System.out.println("Your guess:");
			char guess = reader.nextLine().charAt(0);
			guess = toUpperCase(guess);
			
			if(isLetterInWord(word, guess) != -1){
				lettersPosition[isLetterInWord(word, guess)] = true;
			}
			else {
				System.out.println("That letter is not in the word :( Attempts left: " + attempts);
				attempts --;
			}
			
			printWork(word, lettersPosition);
			for(int i = word.length() - 1; i >= 0; i--){
				targetReached = targetReached && lettersPosition[i];
			}
		}
		if(targetReached){
			System.out.println("Congrats! You guessed the word, it was " + word);
		}
		else System.out.println("You didn't make it :( The word was " + word);
	}
}