import java.util.Scanner;

public class Driver {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Hi there! Welcome to this calculator :)");
		
		System.out.println("Please input two numbers to add together, starting with the first one:");
		int add1 = reader.nextInt();
		reader.nextLine();
		System.out.println("Now the second one:");
		int add2 = reader.nextInt();
		reader.nextLine();
		System.out.println("The result: " + Calculator.add(add1, add2));
		
		System.out.println("Now input a number for which you want to obtain the square root:");
		int sqrt = reader.nextInt();
		System.out.println("Square root of " + sqrt + " is " + Calculator.sqrt(sqrt));
		
		System.out.println("Lastly, here is a random number: " + Calculator.random());
	}
}